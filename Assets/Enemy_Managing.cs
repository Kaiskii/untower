﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy_Managing : MonoBehaviour
{
    [SerializeField] Enemy_Spawning UpperLeft;
    [SerializeField] Enemy_Spawning UpperRight;
    [SerializeField] Enemy_Spawning LowerLeft;
    [SerializeField] Enemy_Spawning LowerRight;

    [SerializeField] Enemy_Spawning S1;
    [SerializeField] Enemy_Spawning S2;
    [SerializeField] Enemy_Spawning S3;
    [SerializeField] Enemy_Spawning S4;
    [SerializeField] Enemy_Spawning S5;
    [SerializeField] Enemy_Spawning S6;

    [SerializeField] GameObject wispNormal;
    [SerializeField] GameObject wispAngry;
    [SerializeField] GameObject miniTaur;
    [SerializeField] GameObject golem;

    [SerializeField] bool isWaveComplete = true;
    int waveIndex = 1;

    [SerializeField] Text waveText;
    string textToDisplay;


    private void Update()
    {
        if (waveIndex <= 4)
            isWaveComplete = CheckWaveEnd(false);
        else
            isWaveComplete = CheckWaveEnd(true);

        if (isWaveComplete == true)
        {
            waveText.text = "Wave " + waveIndex;
            waveText.GetComponent<Animator>().SetTrigger("NewWave");

            ResetSpawners();
            FetchWave(waveIndex);

            waveIndex++;
        }
    }

    void BatchAdd(GameObject monsterToAdd,int UL, int UR, int LR, int LL,int s5, int s6,bool waveToggle,int baseAmount)
    {
        if (!waveToggle)
        {
            if (UL > 0)
                UpperLeft.AddEnemyBatch(monsterToAdd, UL);
            else
                UpperLeft.AddEnemyBatch(null, baseAmount);

            if (UR > 0)
                UpperRight.AddEnemyBatch(monsterToAdd, UR);
            else
                UpperRight.AddEnemyBatch(null, baseAmount);

            if (LR > 0)
                LowerRight.AddEnemyBatch(monsterToAdd, LR);
            else
                LowerRight.AddEnemyBatch(null, baseAmount);

            if (LL > 0)
                LowerLeft.AddEnemyBatch(monsterToAdd, LL);
            else
                LowerLeft.AddEnemyBatch(null, baseAmount);
        }
        else
        {
            if (UL > 0)
                S1.AddEnemyBatch(monsterToAdd, UL);
            else
                S1.AddEnemyBatch(null, baseAmount);

            if (UR > 0)
                S2.AddEnemyBatch(monsterToAdd, UR);
            else
                S2.AddEnemyBatch(null, baseAmount);

            if (LR > 0)
                S3.AddEnemyBatch(monsterToAdd, LR);
            else
                S3.AddEnemyBatch(null, baseAmount);

            if (LL > 0)
                S4.AddEnemyBatch(monsterToAdd, LL);
            else
                S4.AddEnemyBatch(null, baseAmount);

            if (s5 > 0)
                S5.AddEnemyBatch(monsterToAdd, LL);
            else
                S5.AddEnemyBatch(null, baseAmount);

            if (s6 > 0)
                S6.AddEnemyBatch(monsterToAdd, LL);
            else
                S6.AddEnemyBatch(null, baseAmount);
        }
    }

    bool CheckWaveEnd(bool doExtended)
    {
        if (!doExtended)
        {
            if (UpperLeft.GetSpawnerComplete() && UpperRight.GetSpawnerComplete() && LowerLeft.GetSpawnerComplete() && LowerRight.GetSpawnerComplete())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (S1.GetSpawnerComplete() && S2.GetSpawnerComplete()&& S3.GetSpawnerComplete()&& S4.GetSpawnerComplete()&& S5.GetSpawnerComplete()&& S6.GetSpawnerComplete())
        {
            return true;
        }
        else return false;
    }

    void ResetSpawners()
    {
        UpperLeft.endOfWave = false;
        UpperLeft.index = 0;
        UpperLeft.enemyGroups.Clear();
        UpperLeft.AddEnemyBatch(null, 5);

        UpperRight.endOfWave = false;
        UpperRight.index = 0;
        UpperRight.enemyGroups.Clear();
        UpperRight.AddEnemyBatch(null, 5);

        LowerLeft.endOfWave = false;
        LowerLeft.index = 0;
        LowerLeft.enemyGroups.Clear();
        LowerLeft.AddEnemyBatch(null, 5);

        LowerRight.endOfWave = false;
        LowerRight.index = 0;
        LowerRight.enemyGroups.Clear();
        LowerRight.AddEnemyBatch(null, 5);

        S1.endOfWave = false;
        S1.index = 0;
        S1.enemyGroups.Clear();
        S1.AddEnemyBatch(null, 5);

        S2.endOfWave = false;
        S2.index = 0;
        S2.enemyGroups.Clear();
        S2.AddEnemyBatch(null, 5);

        S3.endOfWave = false;
        S3.index = 0;
        S3.enemyGroups.Clear();
        S3.AddEnemyBatch(null, 5);

        S4.endOfWave = false;
        S4.index = 0;
        S4.enemyGroups.Clear();
        S4.AddEnemyBatch(null, 5);

        S5.endOfWave = false;
        S5.index = 0;
        S5.enemyGroups.Clear();
        S5.AddEnemyBatch(null, 5);

        S6.endOfWave = false;
        S6.index = 0;
        S6.enemyGroups.Clear();
        S6.AddEnemyBatch(null, 5);
    }

    void FetchWave(int index)
    {
        if (index == 1)
        {
            //Wave 1
            BatchAdd(miniTaur, 2, 0, 4, 0, 0, 0, false, 0);
            BatchAdd(null, 0, 0, 0, 0, 0, 0, false, 60);
        }
        else if (index == 2)
        {
            //Wave 2
            BatchAdd(miniTaur, 4, 0, 4, 0, 0, 0, false, 0);
            BatchAdd(null, 0, 0, 0, 0, 0, 0, false, 30);

            BatchAdd(golem, 0, 2, 0, 2, 0, 0, false, 0);
            BatchAdd(null, 0, 0, 0, 0, 0, 0, false, 60);
        }
        else if (index == 3)
        {
            //Wave 3
            BatchAdd(wispNormal, 3, 3, 3, 3, 0, 0, false, 3);
            BatchAdd(null, 0, 0, 0, 0, 0, 0, false, 30);

            BatchAdd(miniTaur, 3, 3, 0, 0, 0, 0, false, 3);
            BatchAdd(golem, 0, 0, 3, 3, 0, 0, false, 3);
            BatchAdd(null, 0, 0, 0, 0, 0, 0, false, 60);
        }
        else if (index == 4)
        {
            //Wave 4
            BatchAdd(miniTaur, 5, 0, 5, 0, 0, 0, false, 5);
            BatchAdd(null, 0, 0, 0, 0, 0, 0, false, 10);

            BatchAdd(miniTaur, 4, 0, 4, 0, 0, 0, false, 4);
            BatchAdd(golem, 0, 4, 0, 4, 0, 0, false, 4);
            BatchAdd(null, 0, 0, 0, 0, 0, 0, false, 20);

            BatchAdd(wispNormal, 2, 2, 2, 2, 0, 0, false, 2);
            BatchAdd(wispAngry, 1, 1, 1, 1, 0, 0, false, 1);
            BatchAdd(wispNormal, 2, 2, 2, 2, 0, 0, false, 2);
            BatchAdd(null, 0, 0, 0, 0, 0, 0, false, 30);

            BatchAdd(miniTaur, 3, 3, 0, 0, 0, 0, false, 3);
            BatchAdd(golem, 0, 0, 3, 3, 0, 0, false, 3);
            BatchAdd(null, 0, 0, 0, 0, 0, 0, false, 60);
        }
        else if (index == 5)
        {
            BatchAdd(miniTaur, 4, 4, 4, 4, 4, 4, true, 4);
            BatchAdd(golem, 3, 3, 3, 0, 0, 0, true, 3);
            BatchAdd(wispNormal, 0, 0, 0, 5, 5, 5, true, 5);
            BatchAdd(wispAngry, 1, 1, 1, 0, 0, 0, true, 1);
            BatchAdd(null, 0, 0, 0, 0, 0, 0, true, 60);
        }
        else if (index == 6)
        {
            BatchAdd(miniTaur, 4, 4, 4, 4, 4, 4, true, 4);
            BatchAdd(golem, 1, 1, 1, 1, 1, 1, true, 1);
            BatchAdd(null, 0, 0, 0, 0, 0, 0, false, 20);

            BatchAdd(miniTaur, 4, 4, 4, 4, 4, 4, true, 4);
            BatchAdd(golem, 1, 1, 1, 1, 1, 1, true, 1);
            BatchAdd(null, 0, 0, 0, 0, 0, 0, true, 60);
        }
        else if (index == 7)
        {
            BatchAdd(miniTaur, 2, 2, 2, 2, 2, 2, true, 2);
            BatchAdd(golem, 1, 1, 1, 1, 1, 1, true, 1);
            BatchAdd(null, 0, 0, 0, 0, 0, 0, false, 20);

            BatchAdd(wispNormal, 3, 3, 3, 3, 3, 3, true, 3);
            BatchAdd(wispAngry, 1, 0, 1, 0, 1, 0, true, 1);
            BatchAdd(miniTaur, 3, 3, 3, 3, 3, 3, true, 2);
            BatchAdd(golem, 2, 2, 2, 2, 2, 2, true, 1);
            BatchAdd(null, 0, 0, 0, 0, 0, 0, true, 60);
        }
        else if (index == 8)
        {
            BatchAdd(wispNormal, 2, 2, 2, 2, 2, 2, true, 1);
            BatchAdd(wispAngry, 1, 0, 1, 0, 1, 0, true, 1);
            BatchAdd(golem, 1, 1, 1, 1, 1, 1, true, 1);
            BatchAdd(miniTaur, 1, 1, 1, 1, 1, 1, true, 1);
            BatchAdd(null, 0, 0, 0, 0, 0, 0, false, 20);

            BatchAdd(wispNormal, 2, 2, 2, 2, 2, 2, true, 1);
            BatchAdd(wispAngry, 0, 1, 0, 1, 0, 1, true, 1);
            BatchAdd(golem, 1, 1, 1, 1, 1, 1, true, 1);
            BatchAdd(miniTaur, 4, 4, 4, 4, 4, 4, true, 1);
            BatchAdd(wispNormal, 0, 3, 0, 0, 3, 0, true, 3);
            BatchAdd(null, 0, 0, 0, 0, 0, 0, true, 60);
        }
        else
        {
            int rand1 = Random.Range(0, 5);
            int rand2 = Random.Range(0, 5);
            int rand3 = Random.Range(0, 5);

            BatchAdd(miniTaur, rand1, rand2, rand3, rand1, rand2, rand3, true, 10);
            BatchAdd(golem, rand2, rand1, rand3, rand2, rand1, rand3, true, 10);
            BatchAdd(wispNormal, rand3, rand2, rand1, rand3, rand2, rand1, true, 10);
            BatchAdd(wispAngry, rand1, rand2, rand3, rand1, rand2, rand3, true, 10);

            BatchAdd(null, 0, 0, 0, 0, 0, 0, true, 60);
        }
    }
}
