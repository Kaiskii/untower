﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnetic_Block : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (transform.GetChild(0).GetComponent<BuffScript>().BT == BuffTypes.MAGNETIC)
        {
            if (col.collider.CompareTag("Turret") || col.collider.CompareTag("Buff"))
            {
                if (col.collider.transform.parent != null)
                {
                    Transform temp = col.collider.transform;
                    while (temp.parent != null)
                    {
                        temp = temp.parent;
                    }
                    temp.parent = transform;
                }
                else
                {
                    col.collider.transform.parent = transform;
                }
            }
        }

    }
}
