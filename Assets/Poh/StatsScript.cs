﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TurretType {
    PROJECTILE = 0,
    LASER,
    MORTA,
    EXTRACTOR,
    DAMAGE_BUFF,
    ICE_BUFF,
    MAGNETIC_BUFF,
    RECOIL_BUFF,
    SPEED_BUFF
}

public class StatsScript : MonoBehaviour {
    [SerializeField] int mineral = 0;
    [SerializeField] int gas = 0;
    int upkeep = 0;
    [SerializeField] Texture2D ClickedSprite;
    [SerializeField] Texture2D UnclickedSprite;
    [SerializeField] List<GameObject> towers = new List<GameObject>();
    [SerializeField] int[] mineralCosts = new int[10];
    [SerializeField] int[] gasCosts = new int[2];
    [SerializeField] int[] upKeeps = new int[10];
    GameObject spawnBase;
    int totalUpkeep = 0;
    int extractorCount = 1;
    [SerializeField] Text mineralNum;
    [SerializeField] Text gasNum;
    [SerializeField] Text[] towerCosts;
    [SerializeField] GameObject[] buttons;
    ResourcesScript[] existingResources;
    [SerializeField] Text upKeepText;
    AudioManager AM;


    public void SetResource(int type, int value) {
        if (type == 0) {
            mineral += value;
        } else {
            gas += value;
        }
    }
    // Start is called before the first frame update
    void Start() {
        spawnBase = GameObject.FindGameObjectWithTag("Base");
        Cursor.SetCursor(UnclickedSprite, Vector2.zero, CursorMode.Auto);
        existingResources = FindObjectsOfType<ResourcesScript>();
        AM = FindObjectOfType<AudioManager>();
    }

    public void SetCustomCursor(bool isClicked) {
        if (isClicked) {
            Cursor.SetCursor(ClickedSprite, Vector2.zero, CursorMode.Auto);
        } else {
            Cursor.SetCursor(UnclickedSprite, Vector2.zero, CursorMode.Auto);
        }

    }

    void CheckSpawnable()
    {
        for(int i =0;i<buttons.Length;i++)
        {
            if(mineral<mineralCosts[i]||gas<gasCosts[i])
            {
                buttons[i].GetComponent<Image>().color = Color.grey;
            }
            else if (mineral >= mineralCosts[i] && gas >= gasCosts[i])
            {
                buttons[i].GetComponent<Image>().color = Color.white;
            }
        }
    }

    public int GetMineral() {
        return mineral;
    }

    public int GetGas() {
        return gas;
    }

    public void ReduceResource(bool isMineral, int value) {
        if (isMineral) {
            mineral -= value;
        } else {
            gas -= value;
        }
    }

    public void Spawn(int numType) {
        TurretType type = (TurretType)numType;
        Debug.Log(type);
        switch (type) {
            case TurretType.PROJECTILE:
                if (mineral >= mineralCosts[0])
                {
                    AM.Play("Purchase");
                    mineral -= mineralCosts[0];
                    totalUpkeep += upKeeps[0];
                    //Spawn the building at spawnpoint
                    Instantiate(towers[0], spawnBase.transform.position + (Vector3.up * 4), Quaternion.identity);
                }
                break;
            case TurretType.LASER:
                if (mineral >= mineralCosts[1] && gas >= gasCosts[1]) {
                    AM.Play("Purchase");
                    mineral -= mineralCosts[1];
                    gas -= gasCosts[1];
                    totalUpkeep += upKeeps[1];
                    //Spawn the building at spawnpoint
                    Instantiate(towers[1], spawnBase.transform.position + (Vector3.up * 4), Quaternion.identity);
                }
                break;
            case TurretType.MORTA:
                if (mineral >= mineralCosts[2] && gas >= gasCosts[2])
                {
                    AM.Play("Purchase");
                    mineral -= mineralCosts[2];
                    gas -= gasCosts[2];
                    totalUpkeep += upKeeps[2];
                    //Spawn the building at spawnpoint
                    Instantiate(towers[2], spawnBase.transform.position + (Vector3.up * 4), Quaternion.identity);
                }
                break;
            case TurretType.EXTRACTOR:
                if (mineral >= mineralCosts[3])
                {
                    AM.Play("Purchase");
                    mineral -= mineralCosts[3];
                    totalUpkeep += upKeeps[3];
                    //Spawn the building at spawnpoint
                    Instantiate(towers[3], spawnBase.transform.position + (Vector3.up * 4), Quaternion.identity);
                    mineralCosts[3] = 250 * Fibonnaci(extractorCount);
                    extractorCount++;

                }
                break;
            case TurretType.DAMAGE_BUFF:
                if (mineral >= mineralCosts[4])
                {
                    AM.Play("Purchase");
                    mineral -= mineralCosts[4];
                    totalUpkeep += upKeeps[4];
                    //Spawn the building at spawnpoint
                    Instantiate(towers[4], spawnBase.transform.position + (Vector3.up * 4), Quaternion.identity).transform.GetChild(0).GetComponent<BuffScript>().BT = BuffTypes.DAMAGE;

                }
                break;
            case TurretType.ICE_BUFF:
                if (mineral >= mineralCosts[5] && gas >= gasCosts[5])
                {
                    AM.Play("Purchase");
                    mineral -= mineralCosts[5];
                    gas -= gasCosts[5];
                    totalUpkeep += upKeeps[5];
                    //Spawn the building at spawnpoint
                    Instantiate(towers[4], spawnBase.transform.position + (Vector3.up * 4), Quaternion.identity).transform.GetChild(0).GetComponent<BuffScript>().BT = BuffTypes.ICE;

                }
                break;
            case TurretType.MAGNETIC_BUFF:
                if (mineral >= mineralCosts[6] && gas >= gasCosts[6])
                {
                    AM.Play("Purchase");
                    mineral -= mineralCosts[6];
                    gas -= gasCosts[6];
                    totalUpkeep += upKeeps[6];            //Spawn the building at spawnpoint
                    Instantiate(towers[4], spawnBase.transform.position + (Vector3.up * 4), Quaternion.identity).transform.GetChild(0).GetComponent<BuffScript>().BT = BuffTypes.MAGNETIC;

                }
                break;
            case TurretType.RECOIL_BUFF:
                if (mineral >= mineralCosts[7])
                {
                    AM.Play("Purchase");
                    mineral -= mineralCosts[7];
                    totalUpkeep += upKeeps[7];
                    //Spawn the building at spawnpoint
                    Instantiate(towers[4], spawnBase.transform.position + (Vector3.up * 4), Quaternion.identity).transform.GetChild(0).GetComponent<BuffScript>().BT = BuffTypes.RECOIL;

                }
                break;
            case TurretType.SPEED_BUFF:
                if (mineral >= mineralCosts[8] && gas >= gasCosts[8])
                {
                    AM.Play("Purchase");
                    mineral -= mineralCosts[8];
                    gas -= gasCosts[8];
                    totalUpkeep += upKeeps[8];
                    //Spawn the building at spawnpoint
                    Instantiate(towers[4], spawnBase.transform.position + (Vector3.up * 4), Quaternion.identity).transform.GetChild(0).GetComponent<BuffScript>().BT = BuffTypes.SPEED;

                }
                break;
        }
    }

    int Fibonnaci(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        }

        return Fibonnaci(n - 1) + Fibonnaci(n - 2);
    }

    // Update is called once per frame
    void Update()
    {
        upKeepText.text = totalUpkeep.ToString();
        if (totalUpkeep >= 0 && totalUpkeep  < 60)
        {
            upKeepText.color = Color.white;
            for (int i = 0; i < existingResources.Length; i ++)
            {
                if(existingResources[i].type == 0)
                {
                    existingResources[i].value = 4;
                }else if(existingResources[i].type == 1)
                {
                    existingResources[i].value = 3;
                }
            }
        }else if(totalUpkeep>= 60 && totalUpkeep < 110)
        {
            upKeepText.color = Color.magenta;
            for (int i = 0; i < existingResources.Length; i++)
            {
                if (existingResources[i].type == 0)
                {
                    existingResources[i].value = 3;
                }
                else if (existingResources[i].type == 1)
                {
                    existingResources[i].value = 2;
                }
            }
        }
        else if (totalUpkeep >= 110)
        {
            upKeepText.color = Color.red;
            for (int i = 0; i < existingResources.Length; i++)
            {
                if (existingResources[i].type == 0)
                {
                    existingResources[i].value = 2;
                }
                else if (existingResources[i].type == 1)
                {
                    existingResources[i].value = 1;
                }
            }
        }
        CheckSpawnable();
        mineralNum.text = mineral.ToString();
        gasNum.text = gas.ToString();
        towerCosts[0].text = mineralCosts[3].ToString() + "  " + gasCosts[3].ToString();
        towerCosts[1].text = mineralCosts[0].ToString() + "  " + gasCosts[0].ToString();
        towerCosts[2].text = mineralCosts[2].ToString() + "  " + gasCosts[2].ToString();
        towerCosts[3].text = mineralCosts[1].ToString() + "  " + gasCosts[1].ToString();
        towerCosts[4].text = mineralCosts[5].ToString() + "  " + gasCosts[5].ToString();
        towerCosts[5].text = mineralCosts[6].ToString() + "  " + gasCosts[6].ToString();
        towerCosts[6].text = mineralCosts[7].ToString() + "  " + gasCosts[7].ToString();
        towerCosts[7].text = mineralCosts[8].ToString() + "  " + gasCosts[8].ToString();
        towerCosts[8].text = mineralCosts[4].ToString() + "  " + gasCosts[4].ToString();


        //Debug.Log("Mineral: " + mineral);
        //Debug.Log("Gas: "+ gas);
    }
}
