﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtractorBehaviour : MonoBehaviour
{
    [SerializeField] GameObject statsTracker;
    [SerializeField] Sprite foundSprite;
    [SerializeField] Sprite notFoundSprite;
    List<Collider2D> resources = new List<Collider2D>();
    [SerializeField] float extractArea = 3.0f;
    [SerializeField] static int mineralCost = 20;
    [SerializeField] static int upKeep = 1;

    public float gainRate = 0.4f;
    public float currentRate = 0.0f;

    Animator extAnim;
    SpriteRenderer extSpr;
    StatsScript extStat;
    AudioManager AM;

    void Start() {
        extAnim = GetComponent<Animator>();
        extSpr = GetComponent<SpriteRenderer>();
        extStat = FindObjectOfType<StatsScript>();

        AM = FindObjectOfType<AudioManager>();
    }

    private void Update()
    {

        if (Physics2D.OverlapCircleAll(transform.position, extractArea, LayerMask.GetMask("ResourceObjects")).Length > 0)
        {
            resources = new List<Collider2D>(Physics2D.OverlapCircleAll(transform.position, extractArea, LayerMask.GetMask("ResourceObjects")));
        }
        else
        {
            resources.Clear();
        }

        if (currentRate <= 0 && resources.Count > 0)
        {
            for (int i = 0; i < resources.Count; i++)
            {
                if (resources[i].GetComponent<ResourcesScript>().type == 0)
                {
                    extStat.SetResource(0, resources[i].GetComponent<ResourcesScript>().value);
                }
                else if (resources[i].GetComponent<ResourcesScript>().type == 1)
                {
                    extStat.SetResource(1, resources[i].GetComponent<ResourcesScript>().value);
                }
                currentRate = gainRate;
            }

        }
        else
        {
            currentRate -= Time.deltaTime;
        }

        if (extSpr.sprite != foundSprite && resources.Count > 0)
        {
            extSpr.sprite = foundSprite;
            extAnim.SetBool("isDrill", true);
        }
        else if(extSpr.sprite != notFoundSprite && resources.Count <= 0)
        {
            extSpr.sprite = notFoundSprite;
            extAnim.SetBool("isDrill", false);
        }


       
    }

    public void OnCollisionEnter2D(Collision2D col) {
        if (col.transform.CompareTag("Enemy")) {
            Explode();
        }
    }


    public void Explode()
    {
        for(int i =0;i<resources.Count;i++)
        {
            Vector3 direction = resources[i].transform.position;
            direction.z = 0f;

            direction.x = direction.x - transform.position.x;
            direction.y = direction.y - transform.position.y;

            resources[i].GetComponent<Rigidbody2D>().AddForce(new Vector2(direction.x, direction.y) * 50.0f);
        }

        AM.Play("Explosion4");
        Destroy(gameObject);
    }
}
