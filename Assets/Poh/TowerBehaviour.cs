﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBehaviour : Projectile_Base {
    public bool isEnabled = true;

    [SerializeField]GameObject projectile;
    List<Collider2D> targets;
    GameObject mainTarget;
    Transform TurretHead;
    Rigidbody2D rb;
    Vector3 targ;
    Vector3 knockbackDir;
    AudioManager AM;


    // Start is called before the first frame update
    void Start()
    {
        targets = new List<Collider2D>();
        rb = this.GetComponent<Rigidbody2D>();
        TurretHead = transform.GetChild(0);
        AM = FindObjectOfType<AudioManager>();
    }

    // Update is called once per frame
    void Update() {
        if (isEnabled) {
            if (Physics2D.OverlapCircleAll(transform.position, attackRadius, LayerMask.GetMask("EnemyObjects")).Length > 0) {
                targets = new List<Collider2D>(Physics2D.OverlapCircleAll(transform.position, attackRadius, LayerMask.GetMask("EnemyObjects")));
                if (mainTarget == null) {
                    mainTarget = targets[0].gameObject;
                } else if (!targets.Contains(mainTarget.GetComponent<Collider2D>())) {
                    mainTarget = targets[0].gameObject;
                }
            }
            else
            {
                if (targets != null)
                {
                    mainTarget = null;
                    targets.Clear();
                }
            }

            if (currentFireRate <= 0.0f && mainTarget != null) {
                int randomInt = Random.Range(1, 3);
                AM.Play("Projectile" + randomInt.ToString());
                GameObject tempProjectile = Instantiate(projectile, TurretHead.position, TurretHead.localRotation);
                tempProjectile.GetComponent<ProjectileBehaviour>().targetTrans = mainTarget.transform.position;
                tempProjectile.GetComponent<ProjectileBehaviour>().isSlow = this.isSlow;
                targ = mainTarget.transform.position;
                targ.z = 0f;

                targ.x = targ.x - TurretHead.position.x;
                targ.y = targ.y - TurretHead.position.y;

                float angle = Mathf.Atan2(targ.y, targ.x) * Mathf.Rad2Deg;
                TurretHead.rotation = Quaternion.Euler(new Vector3(0, 0, angle - 90));

                rb.AddForce(new Vector2(-targ.x, -targ.y) * knockbackForce, ForceMode2D.Impulse);
                currentFireRate = fireRate;

                Physics2D.IgnoreCollision(tempProjectile.GetComponent<Collider2D>(), GetComponent<Collider2D>());
            } else {
                currentFireRate -= Time.deltaTime;
            }

        }

    }

    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, attackRadius);
    }
}
