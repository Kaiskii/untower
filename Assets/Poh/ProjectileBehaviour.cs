﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBehaviour : MonoBehaviour
{
    public GameObject target;
    public Vector3 targetTrans = Vector3.zero;
    public Vector3 targ;
    Vector3 dir = Vector3.zero;
    public float moveSpeed = 3.0f;
    public float damage = 0.0f;

    public float Lifespan = 5.0f;

    public bool isSlow = false;


    // Update is called once per frame
    private void Start()
    {
        if (targetTrans != Vector3.zero)
        {
            dir = targetTrans - transform.position;
            dir = dir.normalized *  moveSpeed;
        }

        GameObject holder = GameObject.Find("ProjectileHolder");
        transform.SetParent(holder.transform);
    }

    void Update()
    {
        if (target != null)
            transform.position = Vector2.MoveTowards(transform.position, target.transform.position, Time.deltaTime * moveSpeed);
        else if (target == null & targetTrans == Vector3.zero)
            Destroy(gameObject);

        if (Lifespan <= 0)
            Destroy(gameObject);

        Lifespan -= Time.deltaTime;
        transform.position += dir * Time.deltaTime;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            collision.gameObject.GetComponent<Enemy_Movement>().ApplyDamage(damage, isSlow);

            Destroy(gameObject);
        }
    }


}
