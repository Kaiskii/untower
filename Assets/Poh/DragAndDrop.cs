﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(BoxCollider2D))]

public class DragAndDrop : MonoBehaviour
{
    private Vector3 screenPoint;
    private Vector3 offset;
    Vector3 dragPos;
    Vector3 endPos;
    StatsScript statsTracker;
    float timer = 0.0f;
    [SerializeField]float cooldown = 1.0f;
    float curCooldown = 0.0f;

    bool mouseHover = false;
    bool isDragging = false;

    SpriteRenderer rend;

    TowerBehaviour projectileTurret;
    Turret_Laser laserTurret;
    Turret_Mortar mortarTurret;
    BuffScript buffBlocks;

    private void Start()
    {
        statsTracker = FindObjectOfType<StatsScript>();
        rend = GetComponent<SpriteRenderer>();

        projectileTurret = GetComponent<TowerBehaviour>();
        laserTurret = GetComponent<Turret_Laser>();
        mortarTurret = GetComponent<Turret_Mortar>();
        buffBlocks = GetComponent<BuffScript>();
    }
    

    private void Update()
    {
        if(curCooldown > 0)
        {
            rend.color = Color.red;
            curCooldown -= Time.deltaTime;
        }
        else
        {
            if (rend.color != Color.white && rend.color != Color.grey) {
                if (projectileTurret != null) projectileTurret.isEnabled = true;
                if (laserTurret != null) laserTurret.isEnabled = true;
                if (mortarTurret != null) mortarTurret.isEnabled = true;
                if (buffBlocks != null) buffBlocks.isEnabled = true;
                if (mouseHover == false) {
                    rend.color = Color.white;
                }
            }
        }
    }

    void OnMouseDown()
    {
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        statsTracker.SetCustomCursor(true);

        if (projectileTurret != null) projectileTurret.isEnabled = false;
        if (laserTurret != null) laserTurret.isEnabled = false;
        if (mortarTurret != null) mortarTurret.isEnabled = false;
        if (buffBlocks != null) buffBlocks.isEnabled = false;

        timer = 0.0f;
    }

    void OnMouseDrag()
    {
        if (projectileTurret != null) projectileTurret.isEnabled = false;
        if (laserTurret != null) laserTurret.isEnabled = false;
        if (mortarTurret != null) mortarTurret.isEnabled = false;
        if (buffBlocks != null) buffBlocks.isEnabled = false;

        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        transform.position = curPosition;
        if(timer <= 0)
        {
            dragPos = curPosition;
            timer = 0.5f;
        }
        else
        {
            timer -= Time.deltaTime;
        }
        GetComponent<Rigidbody2D>().velocity = Vector3.zero;
    }

    private void OnMouseEnter()
    {
        mouseHover = true;
        rend.color = Color.grey;
    }

    void OnMouseExit()
    {
        mouseHover = false;
    }

    private void OnMouseUp()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        endPos = curPosition;
        statsTracker.SetCustomCursor(false);
        Vector3 dir = endPos - dragPos;
        GetComponent<Rigidbody2D>().AddForce(dir * 50.0f);


        curCooldown = cooldown;
    }
}
