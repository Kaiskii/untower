﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mortar_ProjectileBehaviour : MonoBehaviour
{
    [SerializeField] float attackRadius;
    [SerializeField] float attackTimer;
    float currentTimer;
    public float attackDamage;
    [SerializeField] float explosionForce;

    public bool isSlow = false;

    [SerializeField] GameObject indicatorRing;

    List<Collider2D> targets;
    AudioManager AM;
    // Start is called before the first frame update
    void Start()
    {
        AM = FindObjectOfType<AudioManager>();
        currentTimer = attackTimer;
        int random = Random.Range(1, 3);
        AM.Play("MortarIncoming" + random.ToString());

        GameObject holder = GameObject.Find("ProjectileHolder");
        transform.SetParent(holder.transform);
    }

    // Update is called once per frame
    void Update()
    {
        indicatorRing.transform.localScale -= new Vector3(.04F, .04f, .04f) * attackTimer * Time.deltaTime;
        currentTimer -= Time.deltaTime;

        if(currentTimer <= 0)
        {
            DetonateMortar();
            int random = Random.Range(1, 3);
            AM.Play("MortarReach" + random.ToString());
            Destroy(this.gameObject);
        }
    }

    void DetonateMortar()
    {
        targets = new List<Collider2D>(Physics2D.OverlapCircleAll(transform.position, attackRadius, LayerMask.GetMask("EnemyObjects")));
        for(int i = 0;i<targets.Count;i++)
        {
            targets[i].GetComponent<Enemy_Movement>().ApplyDamage(attackDamage, isSlow);
        }

        targets = new List<Collider2D>(Physics2D.OverlapCircleAll(transform.position, attackRadius, LayerMask.GetMask("PlayerObjects")));
        Vector3 dir;
        for (int i = 0; i < targets.Count; i++)
        {
            dir = transform.position - targets[i].transform.position;
            dir.Normalize();

            targets[i].GetComponentInParent<Rigidbody2D>().AddForce(-dir * explosionForce, ForceMode2D.Impulse);
        }
    }

    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, attackRadius);
    }
}
