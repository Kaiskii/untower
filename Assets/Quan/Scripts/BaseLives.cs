﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BaseLives : MonoBehaviour
{

    int lives = 50;
    bool gameLost = false;
    public Text textLives;
    AudioManager AM;

    // Start is called before the first frame update
    void Start()
    {

        AM = FindObjectOfType<AudioManager>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(!gameLost & collision.collider.CompareTag("Enemy"))
        {
            AM.Play("UnderAttack");
            lives--;
            Destroy(collision.collider.gameObject);
        }
        if(lives <= 0)
        {
            gameLost = true;
            SceneManager.LoadScene(0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        textLives.rectTransform.position = Camera.main.WorldToScreenPoint(transform.position);
        textLives.text = lives.ToString();
    }
}
