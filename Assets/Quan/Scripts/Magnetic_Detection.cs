﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnetic_Detection : MonoBehaviour
{
    Rigidbody2D RB;
    bool containsMagnetic = false;
    void Start()
    {
        RB = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(RB.velocity.x >= 0.5 || RB.velocity.x <= -0.5 || RB.velocity.y >= 0.5 || RB.velocity.y <= -0.5)
        {
            if (transform.parent != null)
            {
                Transform temp = transform;
                while (temp.parent != null)
                {
                    temp = temp.parent;
                }

                if (temp.GetChild(0).GetComponent<BuffScript>() != null && temp.GetChild(0).GetComponent<BuffScript>().BT == BuffTypes.MAGNETIC)
                {
                    temp.GetComponent<Rigidbody2D>().velocity = RB.velocity;
                }

            }
        }
    }
}
