﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyGroup
{
    public GameObject enemyType;
    public int spawnAmount;
}

public class Enemy_Spawning : MonoBehaviour
{
    [SerializeField] GameObject wispType1;
    [SerializeField] GameObject wispType2;
    public List<Transform> waypoints;
    public List<EnemyGroup> enemyGroups;

    public bool endOfWave = false;
    public int index = 0;

    float tickSpeed = 0.5f;
    float gameClock = 0;
    int spawnCounter = 0;

    private void Start()
    {
        spawnCounter = enemyGroups[0].spawnAmount;
    }
    // Update is called once per frame
    void Update()
    {
        gameClock += Time.deltaTime;
        if (!endOfWave)
        {
            if (gameClock >= tickSpeed)
            {
                gameClock = 0;
                if (spawnCounter > 0)
                {
                    if (enemyGroups[index].enemyType != null)
                    {
                        if(enemyGroups[index].enemyType == wispType1 || enemyGroups[index].enemyType == wispType2)
                        {
                            tickSpeed = 0.1f;
                        }
                        else if (tickSpeed != 0.5f)
                        {
                            tickSpeed = 0.5f;
                        }
                        GameObject newObject = Object.Instantiate(enemyGroups[index].enemyType, this.transform.position, Quaternion.identity);
                        newObject.GetComponent<Enemy_Movement>().AssignWaypoints(waypoints);
                    }

                    spawnCounter--;
                }
                else
                {
                    if (index + 1 < enemyGroups.Count)
                    {
                        index++;
                        spawnCounter = enemyGroups[index].spawnAmount;
                    }
                    else endOfWave = true;
                }
            }
        }
    }

    public void AddEnemyBatch(GameObject enemyToSpawn,int amountToSpawn)
    {
        EnemyGroup newGroup = new EnemyGroup
        {
            enemyType = enemyToSpawn,
            spawnAmount = amountToSpawn
        };

        enemyGroups.Add(newGroup);
    }

    public bool GetSpawnerComplete()
    {
        return endOfWave;
    }
}
