﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret_Laser : Projectile_Base {
    public bool isEnabled = true;

    [SerializeField] GameObject projectile;
    LineRenderer render;

    List<Collider2D> targets;
    GameObject mainTarget;
    Transform TurretHead;
    Rigidbody2D rb;
    Vector3 knockbackDir;

    [SerializeField] float laserPersistRate = 0.1f;
    float currentPersistRate = 0.0f;

    Vector3 targetPosition;
    AudioManager AM;


    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        TurretHead = transform.GetChild(0);

        render = this.GetComponent<LineRenderer>();
        render.positionCount = 2;

        AM = FindObjectOfType<AudioManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isEnabled)
        {
            if (Physics2D.OverlapCircleAll(transform.position, attackRadius, LayerMask.GetMask("EnemyObjects")).Length > 0)
            {
                targets = new List<Collider2D>(Physics2D.OverlapCircleAll(transform.position, attackRadius, LayerMask.GetMask("EnemyObjects")));
                if (mainTarget == null)
                {
                    mainTarget = targets[0].gameObject;
                }
                else if (!targets.Contains(mainTarget.GetComponent<Collider2D>()))
                {
                    mainTarget = targets[0].gameObject;
                }
            }
            else
            {
                if (targets != null)
                {
                    targets.Clear();
                    mainTarget = null;
                }
            }


            if (currentFireRate <= 0.0f && mainTarget != null)
            {
                int randomInt = Random.Range(1, 3);
                AM.Play("Laser" + randomInt.ToString()) ;
                render.positionCount = 2;

                render.SetPosition(0, this.transform.position);
                render.SetPosition(1, mainTarget.transform.position);
                currentPersistRate = laserPersistRate;

                targetPosition = mainTarget.transform.position;
                targetPosition.z = 0f;
                targetPosition.x = targetPosition.x - TurretHead.position.x;
                targetPosition.y = targetPosition.y - TurretHead.position.y;

                float angle = Mathf.Atan2(targetPosition.y, targetPosition.x) * Mathf.Rad2Deg;
                TurretHead.rotation = Quaternion.Euler(new Vector3(0, 0, angle - 90));

                rb.AddForce(new Vector2(-mainTarget.transform.position.x, -mainTarget.transform.position.y) * knockbackForce, ForceMode2D.Impulse);

                mainTarget.GetComponent<Enemy_Movement>().ApplyDamage(projDamage, isSlow);
                currentFireRate = fireRate;


            }
            else currentFireRate -= Time.deltaTime;

            if (currentPersistRate <= 0)
            {
                render.positionCount = 0;
            }
            else currentPersistRate -= Time.deltaTime;
        }
    }

    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, attackRadius);
    }
}
