﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionBehaviour : MonoBehaviour
{
    public float explosionForce = 50.0f;
    AudioManager AM;
    // Update is called once per frame
    void Update()
    {
        AM = FindObjectOfType<AudioManager>();
        //Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log("Pop!");
        //Repel all
        Vector3 dir = transform.position - col.gameObject.transform.position;
        dir.Normalize();
        col.gameObject.GetComponentInParent<Rigidbody2D>().AddForce(-dir * explosionForce, ForceMode2D.Impulse);
    }
}
