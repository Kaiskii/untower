﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class SplatterBehaviour : MonoBehaviour
{
    public float fadeTime = 10;
    SpriteRenderer render;

    // Start is called before the first frame update
    void Start()
    {
        render = this.GetComponent<SpriteRenderer>();
        GameObject particleHolder = GameObject.Find("ParticleHolder");
        transform.SetParent(particleHolder.transform);
    }

    // Update is called once per frame
    void Update()
    {
        Color newColor = render.color;
        newColor.a -= Time.deltaTime / fadeTime;

        render.color = newColor;

        if(render.color.a <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
