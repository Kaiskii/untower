﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectileBehaviour : MonoBehaviour
{
   public Vector3 target;
    //public float distance;
    [SerializeField] GameObject explodeObject;
    [SerializeField] float speed = 5.0f;

    private void Start()
    {
        target += new Vector3(Random.Range(-2.0f, 2.0f), Random.Range(-2.0f, 2.0f));

        GameObject holder = GameObject.Find("ProjectileHolder");
        transform.SetParent(holder.transform);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, target, Time.deltaTime * speed);

        //distance = Vector3.Distance(transform.position, target);
        if (Vector3.Distance(transform.position,target) < 2.0f)
        {
            
            GameObject newobj = Object.Instantiate(explodeObject, this.transform.position, Quaternion.identity);
            newobj.transform.localScale = new Vector2(0.5f, 0.5f);
            newobj.GetComponent<ExplosionBehaviour>().explosionForce = 10;
            Destroy(this.gameObject);
        }

    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        Vector3 dir = transform.position - col.gameObject.transform.position;
        dir.Normalize();
        dir = transform.TransformDirection(dir);

        if(col.gameObject.GetComponent<Rigidbody2D>() != null)
            col.gameObject.GetComponent<Rigidbody2D>().AddForce(-dir*5.0f,ForceMode2D.Impulse);
        Destroy(this.gameObject);
    }
}
