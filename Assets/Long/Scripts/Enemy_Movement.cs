﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Movement : MonoBehaviour
{
    Rigidbody2D rb2d;
    SpriteRenderer rend;
    float hitFlash = 1.0f;
    [SerializeField] CircleCollider2D targetingRadius;
    [SerializeField] GameObject splatterObject;
    [SerializeField] GameObject explodeObject;
    [SerializeField] GameObject projectileObject;

    public bool canShoot = false;
    public float shotCooldown;
    public float currentCooldown;

    public float slowDuration = 1.5f;
    public float curSlowDur = 0.0f;
    [SerializeField] float slowPercVal = 30.0f;

    public bool targetsBuildings = false;
    public string buildingType = "Turret";

    public bool explodesOnDeath = false;

    public float health = 100;
    public float curMoveSpeed;
    public float moveSpeed;
    public float turnSpeed;
    public float attackRange;

    List<Transform> waypoints;
    int waypointIndex = 0;
    public float waypointDistanceTolerance = 3f;

    List<GameObject>targetList = new List<GameObject>();
    Vector3 diff;
    AudioManager AM;

    // Start is called before the first frame update
    void Start()
    {
        curMoveSpeed = moveSpeed;
        curSlowDur = slowDuration;
        rb2d = this.GetComponent<Rigidbody2D>();
        rend = this.GetComponent<SpriteRenderer>();
        targetingRadius.radius = attackRange;
        AM = FindObjectOfType<AudioManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (curSlowDur <= 0) {
            curMoveSpeed = moveSpeed;
        } else {
            curSlowDur -= Time.deltaTime;
        }

        if(rend.color == Color.red)
        {
            if(hitFlash <= 0)
            {
                rend.color = Color.white;
                hitFlash = 0.1f;
            }
            else
            {
                hitFlash -= Time.deltaTime;
            }
        }

        //Turn to direction
        if (targetsBuildings && targetList.Count > 0)
        {
            diff = transform.position - targetList[0].gameObject.transform.position;
        }
        else if(waypoints != null)
        {
            diff = transform.position - waypoints[waypointIndex].position;
        }
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Slerp(transform.rotation,Quaternion.Euler(0f, 0f, rot_z - 90),Time.deltaTime*turnSpeed);

        //Move down
        if (waypoints != null)
        {
            rb2d.AddForceAtPosition(-(transform.up) * curMoveSpeed, this.transform.position);

            if (Vector3.Distance(this.transform.position, waypoints[waypointIndex].position) <= waypointDistanceTolerance)
            {
                if (waypointIndex < waypoints.Count - 1)
                    waypointIndex++;
                else
                    waypointIndex = 0;
            }
        }

        //Lock/unlock on to newest
        if (targetList.Count > 0)
        {
            if (canShoot)
            {
                //Debug.DrawLine(this.transform.position, currentTarget.transform.position, Color.yellow);
                if (currentCooldown <= 0)
                {
                    Shoot();
                    currentCooldown = shotCooldown + Random.Range(-0.5f,0.5f);
                }
                else
                {
                    currentCooldown -= Time.deltaTime;
                }
            }

            if(targetsBuildings && curMoveSpeed != 5)
            {
                curMoveSpeed = 5;
            }
        }
        else if (curMoveSpeed != 1)
        {
            curMoveSpeed = 1;
        }
    }

    public void Shoot()
    {
        if (targetList.Count > 0)
        {
            GameObject proj = Object.Instantiate(projectileObject, this.transform.position, Quaternion.identity);
            proj.GetComponent<EnemyProjectileBehaviour>().target = targetList[0].transform.position;
        }
    }

    public void AssignWaypoints(List<Transform> pointsToAssign)
    {
        waypoints = pointsToAssign;
        waypointIndex = 0;
    }

    public void ApplyDamage(float damageToDeal, bool isSlow)
    {
        health -= damageToDeal;
        rend.color = Color.red;
        hitFlash = 0.1f;

        if (isSlow)
        {
            curSlowDur = slowDuration;
            curMoveSpeed = curMoveSpeed / 100.0f * slowPercVal;
        }

        if (health <= 0)
        {
            //Die
            DoDeathSplatter();

            if (explodesOnDeath)
            {
                int randomInt = Random.Range(1, 3);
                AM.Play("Explosion" + randomInt.ToString());
                Object.Instantiate(explodeObject, this.transform.position, Quaternion.identity);

            }
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == buildingType)
        {
            //if the object is not already in the list
            if (!targetList.Contains(col.gameObject))
            {
                //add the object to the list
                targetList.Add(col.gameObject);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        //if the object is in the list
        if (targetList.Contains(col.gameObject))
        {
            //remove it from the list
            targetList.Remove(col.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (targetsBuildings && col.gameObject.tag == buildingType)
        {
            Debug.Log("Boom!");
            
            Vector3 dir = transform.position - col.gameObject.transform.position;
            dir.Normalize();

            col.gameObject.GetComponentInParent<Rigidbody2D>().AddForce(-dir * 150.0f, ForceMode2D.Impulse);
            DoDeathSplatter();
            Destroy(this.gameObject);
        }
    }

    private void DoDeathSplatter()
    {
        for (int i = 0; i < 10; i++)
        {
            GameObject newObject = Object.Instantiate(splatterObject, this.transform.position, Quaternion.identity);
            float rand = Random.Range(0.1f, 0.5f);
            newObject.transform.localScale = new Vector2(rand,rand);
            newObject.transform.Translate(new Vector2(Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f)));
        }
    }
}
