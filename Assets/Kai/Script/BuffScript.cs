﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BuffTypes {
    DAMAGE = 0,
    ICE,
    MAGNETIC,
    RECOIL,
    SPEED
}

public class BuffScript : MonoBehaviour {

    [SerializeField] Sprite[] buffSprites;
    [SerializeField] List<GameObject> towerList;
    
    public BuffTypes BT = BuffTypes.DAMAGE;

    SpriteRenderer sprR;
    public bool isEnabled;

    float clearDmg, clearRecoil, clearSpeed = 0;

    [Tooltip("All These are 0% - x%")]
    [SerializeField] int damagePercVal, recoilPercVal, speedPercVal = 0;
    [SerializeField] bool isMagnet = false;


    // Start is called before the first frame update
    void Start() {
        sprR = transform.parent.GetComponent<SpriteRenderer>();


        sprR.sprite = buffSprites[(int) BT];
    }

    // Update is called once per frame
    void Update() {
        if (isMagnet)
            return;
            
        if(sprR.sprite != buffSprites[(int) BT])
            sprR.sprite = buffSprites[(int)BT];
    }

    

    void OnTriggerEnter2D(Collider2D col) {
        if (col.CompareTag("Turret") || col.CompareTag("Support")) {
            towerList.Add(col.gameObject);
            AffectBuff(BT, towerList, false,col);

            if(clearDmg <= 0.0f && col.CompareTag("Turret")) {
                clearDmg = col.GetComponent<Projectile_Base>().projDamage / 100.0f * damagePercVal;
            }
        }
    }

    void OnTriggerExit2D(Collider2D col) {
        if (col.CompareTag("Turret") || col.CompareTag("Support") || col.CompareTag("Buff")) {
            AffectBuff(BT, towerList, true,col);
            towerList.Remove(col.gameObject);
        }

        if (clearDmg > 0.0f)
            clearDmg = 0.0f;
    }

    void AffectBuff(BuffTypes bt, List<GameObject> towList, bool clear, Collider2D col) {
        for(int i = 0; i < towList.Count; i++) {

            if (towList[i].CompareTag("Support")) {
                if (bt == BuffTypes.SPEED) {
                    ExtractorBehaviour extB = towList[i].GetComponent<ExtractorBehaviour>();

                    if (clear) {
                        extB.gainRate += clearSpeed;
                    } else {
                        clearSpeed = extB.gainRate / 100.0f * damagePercVal;
                        extB.gainRate -= clearSpeed;
                    }
                    return;
                }
                return;
            }

            Projectile_Base tL = towList[i].GetComponent<Projectile_Base>();


            switch (bt) {
                case BuffTypes.DAMAGE:
                    if (clear)
                        tL.projDamage -= clearDmg;
                    else {
                        tL.projDamage += clearDmg;
                    }

                    break;
                case BuffTypes.ICE:
                    //! TODO: Implement the Damage over Time && Slow;
                    //tL.SetProjBool(bt, !clear);
                    tL.isSlow = !clear;

                    break;
                case BuffTypes.MAGNETIC:
                    if(clear)
                    {
                        foreach (Transform child in transform.parent)
                        {
                            if(child == col.transform)
                            {
                                col.transform.parent = null;
                            }
                        }
                    }

                    break;
                case BuffTypes.RECOIL:
                    if (clear)
                        tL.knockbackForce += clearRecoil;
                    else {
                        clearRecoil = tL.knockbackForce / 100.0f * recoilPercVal;
                        tL.knockbackForce -= clearRecoil;
                    }

                    break;
                case BuffTypes.SPEED:
                    if (clear)
                        tL.fireRate += clearSpeed;
                    else {
                        clearSpeed = tL.fireRate / 100.0f * speedPercVal;
                        tL.fireRate -= clearSpeed;
                    }

                    break;
            }
        }
    }
}
