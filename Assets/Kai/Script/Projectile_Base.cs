﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile_Base : MonoBehaviour
{
    public float knockbackForce = 20.0f;

    public float fireRate = 0.0f;
    public float currentFireRate = 0.0f;

    public float attackRadius = 0.0f;

    public float projDamage = 0.0f;

    public bool isSlow = false;
}
