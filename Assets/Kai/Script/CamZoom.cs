﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class CamZoom : MonoBehaviour{

    public int maxZoomOut = 16;
    public int minZoomOut = 6;
    private void Start()
    {
        if (Camera.main.orthographicSize != maxZoomOut)
            Camera.main.orthographicSize = maxZoomOut;
    }

    // Update is called once per frame
    void Update(){
        Zoom();
    }

    void Zoom()
    {
        if (Input.mouseScrollDelta.y != 0)
        {
            //Debug.Log(Input.mouseScrollDelta.y);
            if (Camera.main.orthographicSize - Input.mouseScrollDelta.y >= minZoomOut && Input.mouseScrollDelta.y > 0)
            {
                Camera.main.orthographicSize -= 2;
            }
            else if (Camera.main.orthographicSize - Input.mouseScrollDelta.y <= maxZoomOut && Input.mouseScrollDelta.y < 0)
            {
                Camera.main.orthographicSize += 2;
            }
        }
    }
}
