﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrillAnimTest : MonoBehaviour {

    Animator DrillAnim;
    // Start is called before the first frame update
    void Start() {
        DrillAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.A)) {
            DrillAnim.SetBool("isDrill", true);
        } else if(Input.GetKeyDown(KeyCode.B)) {
            DrillAnim.SetBool("isDrill",false);
        }
    }
}
