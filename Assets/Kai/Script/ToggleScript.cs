﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleScript : MonoBehaviour
{

    Animator shopAnim;

    bool tempB = false;
    // Start is called before the first frame update
    void Start()
    {
        shopAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1)) {
            tempB = !tempB;
            shopAnim.SetBool("OpenShop", tempB);
        }
    }
}
